# LevelEditor

This is a level editor for isometric games with assets from PV Games, see https://www.patreon.com/PVGames

It lets you create maps, save/load them to .json files, create any number of layers etc.

Some hints:
- Initially you need to choose a folder which contains your tile images. Ideally you have a root folder which contains all of them but you can also add multiple ones via File->Add folder
- Every action can be undone/redone via Ctrl Z / Ctrl Y
- You can resize the map via Edit->Resize
- You can toggle layer visibility using the checkbox in the layers view
- You can zoom using Control +/-
- You can export the currently visible layers to a .png file via File->Export
- Via File->Open/Save/Save as you can manage your levels
- By default snapping is active and set to 32x32. You can deactivate it our change the snap size
- Via Show->Grid you can toggle the grid visibility
- You can use the object filter to search for specific pngs. e.g. enter "wall" and it will only show png files that contain the text "wall"
- In the objects view you can change the scale of the preview items
- The "Max Objects" value in the objects view defines how many objects are visible in the object list. This is to prevent that all images need to be loaded. You can try to set it to 0 which would load all images in your folder(s)
- In the mode "Edit object" you can delete objects(But currently it's hard to figure out where the object is located, this will be reworked for sure)
